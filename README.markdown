#Develop With Passion® - Developer Bootcamp

Prep work for the course!!


##Initial Setup

1. Open up a mingw shell and navigate to this folder.
2. Run the run_first script

  ```bash
  ./run_first
  ```
  This script attempts to bundle install the required ruby gems and then it creates a bunch of branches that will be used for coding. The output for running this script should look similar to this:

  ```bash
  error: Could not remove config section 'remote.jp'      
  Switched to a new branch 'clean'                        
  Already on 'clean'                                      
  fatal: A branch named 'master' already exists.          
  Switched to branch 'master'                             
  Switched to a new branch 'starting_point'               
  Already on 'starting_point'                             
  fatal: A branch named 'master' already exists.          
  Switched to branch 'master'                             
  Switched to a new branch 'codekata'                     
  Already on 'codekata'                                   
  fatal: A branch named 'starting_point' already exists.  
  Switched to branch 'starting_point'                     
  ```
3. Now run the start_new_excercise script (just hit enter when it prompts you for a meaningful branch name):
  ```bash
  ./start_new_excercise
  ```

  The output for this should look similar to this:

  ```bash

  On branch starting_point                                          
  nothing to commit, working directory clean                          
  Enter a meaningful branch name (leave empty if you don't need one)  
                                                                      
  fatal: A branch named 'clean' already exists.                       
  Switched to branch 'clean'                                          
  Switched to a new branch '20131129122555992'                        
  Already on '20131129122555992'                                      
  From http://bitbucket.org/remotedec2013/prep                        
   * branch            master     -> FETCH_HEAD                       
   * [new branch]      master     -> jp/master                        
  Already up-to-date.                                                 
  new branch name:20131129122555992                                   
  ```
4. make sure you have the following section in your ~/.gitconfig file (the only reason you would not is if you missed a step from the initial setup readme) :
  ```bash
  [push]
      default = current
  ```

   This will make sure that you are always pushing to a remote branch with the same name as what you are currently on

5. Open up a mingw shell and navigate to this directory. The do the following:
  ```bash
  echo "Hello" > [your_name_all_lowercase_with_no_spaces].txt
  ./save_my_changes
  ```

  If your git configuration is all setup property (ssh authorization etc along with the config line from above being in your ~/.gitconfig file) you should see output similar to the following:

  ```bash

  warning: LF will be replaced by CRLF in jp.txt.                        
  The file will have its original line endings in your working directory.
  [20131129123450754 0a8ae9d] Pushing new changes                        
  warning: LF will be replaced by CRLF in jp.txt.                        
  The file will have its original line endings in your working directory.
   1 file changed, 1 insertion(+)                                        
   create mode 100644 jp.txt                                             
  Counting objects: 8, done.                                             
  Delta compression using up to 2 threads.                               
  Compressing objects: 100% (5/5), done.                                 
  Writing objects: 100% (6/6), 1006 bytes | 0 bytes/s, done.             
  Total 6 (delta 2), reused 0 (delta 0)                                  
  To git@training:remotedec2013/prep.git                                 
   * [new branch]      20131129123450754 -> 20131129123450754            
   ```

You're all done!! If you run into any issues that you can't seem to resolve, we will address them on the first day of the course.

[Develop With Passion®](http://www.developwithpassion.com)
